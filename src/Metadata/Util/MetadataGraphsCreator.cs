﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace Coscine.Metadata.Util;

/// <summary>
/// Creates metadata graphs in the new structure, if necessary
/// </summary>
public class MetadataGraphsCreator
{
    private readonly MetadataRdfStoreConnector _rdfStoreConnector;

    /// <summary>
    /// Initializes Object
    /// </summary>
    /// <param name="rdfStoreConnector"></param>
    public MetadataGraphsCreator(MetadataRdfStoreConnector rdfStoreConnector)
    {
        _rdfStoreConnector = rdfStoreConnector;
    }

    /// <summary>
    /// Creates and Edits all necessary graphs for the metadata structure
    /// </summary>
    /// <param name="resourceId"></param>
    /// <param name="path"></param>
    /// <param name="createDataVersionGraph"></param>
    /// <param name="createMetadataVersionGraph"></param>
    /// <param name="providedGraphData"></param>
    /// <returns>All edited and created graphs</returns>
    public IEnumerable<IGraph> CreateGraphs(
        string resourceId,
        string path,
        bool createDataVersionGraph = true,
        bool createMetadataVersionGraph = false,
        ProvidedGraphData? providedGraphData = null)
    {
        var graphs = new List<IGraph>();

        var resourceGraphName = $"{Uris.CoscineResourcePrefix}/{resourceId}";

        var newFileGraphName = $"{resourceGraphName}/{path}";

        var version = providedGraphData?.NewVersion ?? VersionUtil.GetNewVersion();

        var newMetadataFileGraphName = $"{newFileGraphName}/@type=metadata";
        var newDataFileGraphName = $"{newFileGraphName}/@type=data";
        var newMetadataVersionFileGraphName = $"{newFileGraphName}/@type=metadata&version={version}";
        var newDataVersionFileGraphName = $"{newFileGraphName}/@type=data&version={version}";

        var newFileGraph = _rdfStoreConnector.GetGraph(newFileGraphName);
        graphs.Add(newFileGraph);

        MetadataUtil.AssertToGraph(newFileGraph, newFileGraph.BaseUri, Uris.A, Uris.DcatCatalogClass);
        MetadataUtil.AssertToGraph(newFileGraph, newFileGraph.BaseUri, Uris.A, Uris.FdpMetadataService);
        AddFilesToAFolder(new Uri(newFileGraphName), resourceGraphName, graphs, providedGraphData);

        var metadataFileGraph = SetMetadataGraph(graphs, newMetadataFileGraphName, newFileGraph, newFileGraphName, providedGraphData);
        var dataFileGraph = SetDataGraph(graphs, newDataFileGraphName, newFileGraph, newFileGraphName, metadataFileGraph.BaseUri.AbsoluteUri, providedGraphData);

        IEnumerable<Uri>? existingGraphs = null;
        if (providedGraphData?.CurrentDataVersionGraph == null || providedGraphData?.CurrentMetadataVersionGraph == null)
        {
            existingGraphs = _rdfStoreConnector.ListGraphsFromTrellisGraph(newFileGraphName + "/");
        }

        if (createDataVersionGraph)
        {
            graphs.AddRange(CreateDataVersionGraph(
                newDataVersionFileGraphName,
                dataFileGraph,
                existingGraphs,
                newFileGraphName,
                providedGraphData
            ));
        }

        if (createMetadataVersionGraph)
        {
            graphs.AddRange(CreateMetadataVersionGraph(
                newMetadataVersionFileGraphName,
                metadataFileGraph,
                existingGraphs,
                newFileGraphName,
                newDataVersionFileGraphName,
                providedGraphData
            ));
        }

        if (providedGraphData?.TrellisGraph != null)
        {
            graphs.Add(providedGraphData.TrellisGraph);
        }

        return graphs;
    }

    /// <summary>
    /// Creates and Edits the extracted metadata graphs
    /// </summary>
    /// <param name="resourceId"></param>
    /// <param name="path"></param>
    /// <param name="givenDataVersion"></param>
    /// <param name="givenMetadataVersion"></param>
    /// <param name="metadataExtractorVersion"></param>
    /// <param name="providedGraphData"></param>
    /// <returns></returns>
    public IEnumerable<IGraph> UpdateExtractionGraphs(
        string resourceId,
        string path,
        string givenDataVersion,
        string givenMetadataVersion,
        string metadataExtractorVersion,
        ProvidedGraphData? providedGraphData = null)
    {
        var resourceGraphName = $"{Uris.CoscineResourcePrefix}/{resourceId}";
        var fileGraphName = $"{resourceGraphName}/{path}";

        var givenDataExtractedVersion = $"{givenDataVersion}&extracted=true";
        var givenMetadataExtractedVersion = $"{givenMetadataVersion}&extracted=true";

        _rdfStoreConnector.AddToTrellis(Uris.LdpRDFSource, fileGraphName, givenDataExtractedVersion, providedGraphData?.TrellisGraph);
        _rdfStoreConnector.AddToTrellis(Uris.LdpRDFSource, fileGraphName, givenMetadataExtractedVersion, providedGraphData?.TrellisGraph);

        var dataFileGraphName = $"{fileGraphName}/@type=data";
        var metadataFileGraphName = $"{fileGraphName}/@type=metadata";

        var dataGraph = _rdfStoreConnector.GetGraph(dataFileGraphName);
        var metadataGraph = _rdfStoreConnector.GetGraph(metadataFileGraphName);

        MetadataUtil.AssertToGraph(dataGraph, new Uri(dataFileGraphName), Uris.DcatDistribution, new Uri(givenDataExtractedVersion));

        // Set information about the data extraction version in the data provenance graph
        MetadataUtil.AssertToGraph(dataGraph, new Uri(givenDataExtractedVersion), Uris.CoscineMetadataExtractionVersion, metadataExtractorVersion);
        MetadataUtil.AssertToGraph(dataGraph, new Uri(givenDataExtractedVersion), Uris.A, Uris.CoscineMetadataExtractionEntity);
        MetadataUtil.AssertToGraph(dataGraph, new Uri(givenDataExtractedVersion), Uris.ProvGeneratedAtTime,
            DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture), new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime));
        MetadataUtil.AssertToGraph(dataGraph, new Uri(givenDataVersion), Uris.CoscineMetadataExtractionExtracted, new Uri(givenDataExtractedVersion));

        MetadataUtil.AssertToGraph(metadataGraph, new Uri(metadataFileGraphName), Uris.FdpHasMetadata, new Uri(givenMetadataExtractedVersion));

        // Set information about the metadata extraction version in the metadata provenance graph
        MetadataUtil.AssertToGraph(metadataGraph, new Uri(givenMetadataExtractedVersion), Uris.CoscineMetadataExtractionVersion, metadataExtractorVersion);
        MetadataUtil.AssertToGraph(metadataGraph, new Uri(givenMetadataExtractedVersion), Uris.A, Uris.CoscineMetadataExtractionEntity);
        MetadataUtil.AssertToGraph(metadataGraph, new Uri(givenMetadataExtractedVersion), Uris.ProvGeneratedAtTime,
            DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture), new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime));
        MetadataUtil.AssertToGraph(metadataGraph, new Uri(givenMetadataVersion), Uris.CoscineMetadataExtractionExtracted, new Uri(givenMetadataExtractedVersion));
        MetadataUtil.AssertToGraph(metadataGraph, new Uri(givenMetadataVersion), Uris.FdpIsMetadataOf, new Uri(givenDataVersion));

        return new List<IGraph> { dataGraph, metadataGraph };
    }

    private void AddFilesToAFolder(Uri fileGraph, string resourceGraphName, List<IGraph> graphs, ProvidedGraphData? providedGraphData)
    {
        string rootUri;
        var fileGraphUri = fileGraph.AbsoluteUri;
        do {
            rootUri = fileGraphUri[..fileGraphUri.LastIndexOf("/")];
            var rootGraph = _rdfStoreConnector.GetEmptySmallUpdateGraph(rootUri);
            MetadataUtil.AssertToGraph(rootGraph, new Uri(rootUri), Uris.DcatCatalog, fileGraph);
            _rdfStoreConnector.AddToTrellis(Uris.LdpBasicContainer, rootUri, fileGraphUri, providedGraphData?.TrellisGraph);
            graphs.Add(rootGraph);
            fileGraphUri = rootUri;
        } while (rootUri != resourceGraphName && rootUri.Contains('/'));
    }

    private IGraph SetMetadataGraph(List<IGraph> graphs, string newMetadataFileGraphName, IGraph newFileGraph, string fileUri, ProvidedGraphData? providedGraphData)
    {
        var metadataUri = new Uri(newMetadataFileGraphName);
        var metadataFileGraph = _rdfStoreConnector.GetEmptySmallUpdateGraph(newMetadataFileGraphName);
        graphs.Add(metadataFileGraph);
        _rdfStoreConnector.AddToTrellis(Uris.LdpRDFSource, fileUri, newMetadataFileGraphName, providedGraphData?.TrellisGraph);
        MetadataUtil.AssertToGraph(newFileGraph, new Uri(fileUri), Uris.DcatCatalog, metadataUri);
        MetadataUtil.AssertToGraph(newFileGraph, new Uri(fileUri), Uris.FdpHasMetadata, metadataUri);
        MetadataUtil.AssertToGraph(metadataFileGraph, metadataUri, Uris.A, Uris.DcatCatalogClass);
        return metadataFileGraph;
    }

    private IGraph SetDataGraph(List<IGraph> graphs, string newDataFileGraphName, IGraph newFileGraph, string fileUri, string metadataFileUriString, ProvidedGraphData? providedGraphData)
    {
        var dataUri = new Uri(newDataFileGraphName);
        var dataFileGraph = _rdfStoreConnector.GetEmptySmallUpdateGraph(newDataFileGraphName);
        graphs.Add(dataFileGraph);
        _rdfStoreConnector.AddToTrellis(Uris.LdpNonRDFSource, fileUri, newDataFileGraphName, providedGraphData?.TrellisGraph);
        MetadataUtil.AssertToGraph(newFileGraph, new Uri(fileUri), Uris.DcatCatalog, dataUri);
        MetadataUtil.AssertToGraph(dataFileGraph, dataUri, Uris.LdpDescribedBy, new Uri(metadataFileUriString));
        MetadataUtil.AssertToGraph(dataFileGraph, dataUri, Uris.A, Uris.DcatCatalogClass);
        return dataFileGraph;
    }

    private IEnumerable<IGraph> CreateDataVersionGraph(string newDataVersionFileGraphName, IGraph dataFileGraph, IEnumerable<Uri>? existingGraphs, string fileUri, ProvidedGraphData? providedGraphData)
    {
        var graphs = new List<IGraph>();

        GetCurrentDataVersionGraph(newDataVersionFileGraphName, existingGraphs, providedGraphData, out IGraph currentDataVersionGraph, out Uri? recentDataVersion);

        if (providedGraphData?.OldDataGraph != null)
        {
            SetLinkedData(providedGraphData.OldDataGraph, currentDataVersionGraph, new Uri(newDataVersionFileGraphName));
        }
        MetadataUtil.AssertToGraph(currentDataVersionGraph, currentDataVersionGraph.BaseUri, Uris.DctermsIdentifier, currentDataVersionGraph.BaseUri.AbsoluteUri);

        // PROV Info
        MetadataUtil.AssertToGraph(dataFileGraph, currentDataVersionGraph.BaseUri, Uris.A, Uris.ProvEntity);
        MetadataUtil.AssertToGraph(dataFileGraph, dataFileGraph.BaseUri, Uris.DcatDataset, currentDataVersionGraph.BaseUri);
        MetadataUtil.AssertToGraph(dataFileGraph, currentDataVersionGraph.BaseUri, Uris.ProvGeneratedAtTime,
            DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture), new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime));

        _rdfStoreConnector.AddToTrellis(Uris.LdpNonRDFSource, fileUri, currentDataVersionGraph.BaseUri.AbsoluteUri, providedGraphData?.TrellisGraph);

        if (recentDataVersion != null && recentDataVersion.AbsoluteUri != currentDataVersionGraph.BaseUri.AbsoluteUri)
        {
            MetadataUtil.AssertToGraph(dataFileGraph, currentDataVersionGraph.BaseUri, Uris.ProvWasRevisionOf, recentDataVersion);
        }

        graphs.Add(currentDataVersionGraph);

        return graphs;
    }

    private void GetCurrentDataVersionGraph(string newDataVersionFileGraphName, IEnumerable<Uri>? existingGraphs, ProvidedGraphData? providedGraphData, out IGraph currentDataVersionGraph, out Uri? recentDataVersion)
    {
        if (providedGraphData?.CurrentDataVersionGraph != null)
        {
            currentDataVersionGraph = providedGraphData.CurrentDataVersionGraph;
            recentDataVersion = providedGraphData.RecentDataVersion;
        }
        else
        {
            recentDataVersion = VersionUtil.GetRecentDataVersion(existingGraphs);

            if (recentDataVersion == null)
            {
                currentDataVersionGraph = new Graph()
                {
                    BaseUri = new Uri(newDataVersionFileGraphName),
                };
            }
            else
            {
                currentDataVersionGraph = _rdfStoreConnector.GetGraph(recentDataVersion);
            }
        }
    }

    private static void SetLinkedData(IGraph linkedGraph, IGraph currentDataVersionGraph, Uri otherURICandidate)
    {
        var linkedBody = currentDataVersionGraph.GetTriplesWithPredicate(Uris.CoscineLinkedBody);
        if (linkedGraph != null)
        {
            var setLinkedBody = linkedGraph.GetTriplesWithPredicate(Uris.CoscineLinkedBody);
            if (!linkedBody.Any()
                    || linkedBody.First().Object.ToString()
                        != setLinkedBody.First().Object.ToString())
            {
                var oldDataVersionNode = currentDataVersionGraph.CreateUriNode(currentDataVersionGraph.BaseUri);
                var currentDataVersionNode = currentDataVersionGraph.CreateUriNode(otherURICandidate);
                currentDataVersionGraph.BaseUri = otherURICandidate;
                foreach (var currentLinkedBody in linkedBody)
                {
                    currentDataVersionGraph.Retract(currentLinkedBody);
                }
                foreach (var oldTriple in currentDataVersionGraph.GetTriplesWithSubject(oldDataVersionNode))
                {
                    currentDataVersionGraph.Retract(oldTriple);
                    currentDataVersionGraph.Assert(new Triple(
                        currentDataVersionNode,
                        Tools.CopyNode(oldTriple.Predicate, currentDataVersionGraph),
                        Tools.CopyNode(oldTriple.Object, currentDataVersionGraph)
                    ));
                }
                foreach (var currentSetLinkedBody in setLinkedBody)
                {
                    currentDataVersionGraph.Assert(new Triple(
                        currentDataVersionNode,
                        currentDataVersionGraph.CreateUriNode(Uris.CoscineLinkedBody),
                        Tools.CopyNode(currentSetLinkedBody.Object, currentDataVersionGraph)
                    ));
                }
            }
        }
    }

    private IEnumerable<IGraph> CreateMetadataVersionGraph(string newMetadataVersionFileGraphName, IGraph metadataFileGraph, IEnumerable<Uri>? existingGraphs, string fileUri, string newDataVersionFileGraphName, ProvidedGraphData? providedGraphData)
    {
        var graphs = new List<IGraph>();

        GetCurrentMetadataVersionGraph(newMetadataVersionFileGraphName, existingGraphs, providedGraphData, out IGraph currentMetadataVersionGraph, out Uri? recentMetadataVersion);
        GetCurrentDataVersionGraph(newDataVersionFileGraphName, existingGraphs, providedGraphData, out IGraph currentDataVersionGraph, out _);

        if (providedGraphData?.OldMetadataGraph != null)
        {
            SetMetadata(providedGraphData.OldMetadataGraph, currentMetadataVersionGraph, new Uri(newMetadataVersionFileGraphName));
        }

        // PROV Info
        MetadataUtil.AssertToGraph(metadataFileGraph, currentMetadataVersionGraph.BaseUri, Uris.A, Uris.ProvEntity);
        MetadataUtil.AssertToGraph(metadataFileGraph, metadataFileGraph.BaseUri, Uris.FdpHasMetadata, currentMetadataVersionGraph.BaseUri);
        MetadataUtil.AssertToGraph(metadataFileGraph, currentMetadataVersionGraph.BaseUri, Uris.ProvGeneratedAtTime,
            DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture), new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime));
        MetadataUtil.AssertToGraph(metadataFileGraph, currentMetadataVersionGraph.BaseUri, Uris.FdpIsMetadataOf, currentDataVersionGraph.BaseUri);

        _rdfStoreConnector.AddToTrellis(Uris.LdpRDFSource, fileUri, currentMetadataVersionGraph.BaseUri.AbsoluteUri, providedGraphData?.TrellisGraph);

        if (recentMetadataVersion != null && recentMetadataVersion.AbsoluteUri != currentMetadataVersionGraph.BaseUri.AbsoluteUri)
        {
            MetadataUtil.AssertToGraph(metadataFileGraph, currentMetadataVersionGraph.BaseUri, Uris.ProvWasRevisionOf, recentMetadataVersion);
        }

        graphs.Add(currentMetadataVersionGraph);

        return graphs;
    }

    private void GetCurrentMetadataVersionGraph(string newMetadataVersionFileGraphName, IEnumerable<Uri>? existingGraphs, ProvidedGraphData? providedGraphData, out IGraph currentMetadataVersionGraph, out Uri? recentMetadataVersion)
    {
        if (providedGraphData?.CurrentMetadataVersionGraph != null)
        {
            currentMetadataVersionGraph = providedGraphData.CurrentMetadataVersionGraph;
            recentMetadataVersion = providedGraphData.RecentMetadataVersion;
        }
        else
        {
            recentMetadataVersion = VersionUtil.GetRecentMetadataVersion(existingGraphs);

            if (recentMetadataVersion == null)
            {
                currentMetadataVersionGraph = new Graph()
                {
                    BaseUri = new Uri(newMetadataVersionFileGraphName),
                };
            }
            else
            {
                currentMetadataVersionGraph = _rdfStoreConnector.GetGraph(recentMetadataVersion);
            }
        }
    }

    private static void SetMetadata(IGraph metadataGraph, IGraph currentMetadataVersionGraph, Uri otherURICandidate)
    {
        // If the predicate & object pairs are different
        if (!
            (metadataGraph.Triples.All((triple) => currentMetadataVersionGraph.GetTriplesWithPredicateObject(triple.Predicate, triple.Object).Any())
            && currentMetadataVersionGraph.Triples.All((triple) => metadataGraph.GetTriplesWithPredicateObject(triple.Predicate, triple.Object).Any())))
        {
            currentMetadataVersionGraph.Retract(currentMetadataVersionGraph.Triples);
            currentMetadataVersionGraph.BaseUri = otherURICandidate;
            var currentMetadataVersionNode = currentMetadataVersionGraph.CreateUriNode(otherURICandidate);
            foreach (var triple in metadataGraph.Triples)
            {
                currentMetadataVersionGraph.Assert(currentMetadataVersionNode, Tools.CopyNode(triple.Predicate, currentMetadataVersionGraph), Tools.CopyNode(triple.Object, currentMetadataVersionGraph));
            }
        }
    }
}