﻿namespace Coscine.Metadata.Models
{
    public class DefaultResourceQuotaModel
    {
        public string ResourceType { get; set; }
        public int DefaultQuota { get; set; }
        public int DefaultMaxQuota { get; set; }
    }
}
