﻿using Coscine.Metadata.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using VDS.RDF.Parsing;
using VDS.RDF.Query;
using VDS.RDF;
using System.Threading.Tasks;

namespace Coscine.Metadata
{
    /// <summary>
    /// Provides additional functionality to interact with versioned metadata
    /// </summary>
    public class MetadataRdfStoreConnector : BaseRdfStoreConnector
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="sparqlEndpoint"></param>
        public MetadataRdfStoreConnector(string sparqlEndpoint = "http://localhost:8890/sparql") : base(sparqlEndpoint)
        {
        }

        /// <summary>
        /// Gets the current data graph of a resource id and path combination.
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="path"></param>
        /// <param name="extracted"></param>
        /// <returns>Current data graph.</returns>
        public string? GetDataId(string resourceId, string path, bool extracted = false)
        {
            var dataIds = GetDataIds(resourceId, path, extracted);

            return dataIds.Max();
        }

        /// <summary>
        /// Gets all data graphs (versions) of a resource id and path combination.
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="path"></param>
        /// <param name="extracted"></param>
        /// <param name="includeInvalidated"></param>
        /// <returns></returns>
        public List<string> GetDataIds(string? resourceId = null, string? path = null, bool extracted = false, bool includeInvalidated = false)
        {
            var resourceGraphUri = $"https://purl.org/coscine/resources/{resourceId}";
            var pathGraphUri = path != null
                ? new Uri($"{resourceGraphUri}/{(path.StartsWith("/") ? path[1..] : path)}/").AbsoluteUri
                : "";

            var query = $@"{{ 
                " + ((resourceId != null) ? $"@resource <{Uris.DcatCatalog}>* ?g ." : "") + $@"
                ?g a <{Uris.FdpMetadataService}> .
                ?g <{Uris.DcatCatalog}> ?m .
                " + (extracted ? $"?m <{Uris.DcatDistribution}> ?v ." : $"?m <{Uris.DcatDataset}> ?v .") + $@"
                " + ((path != null) ? "FILTER(contains(str(?m), @path)) ." : "") + $@"
                " + ((!includeInvalidated) ? $"OPTIONAL {{ ?v <{Uris.ProvWasInvalidatedBy}> ?invalidatedBy }} . " +
                    $"OPTIONAL {{ ?pv <{Uris.ProvWasRevisionOf}>* ?v . ?pv <{Uris.ProvWasInvalidatedBy}> ?invalidatedByParent }} . " +
                    $"FILTER (!bound(?invalidatedBy) && !bound(?invalidatedByParent)) ." : "") + $@"
            }}";

            var cmdString = new SparqlParameterizedString
            {
                CommandText = "SELECT COUNT(?v) AS ?count " + query
            };
            if (resourceId != null) cmdString.SetUri("resource", new Uri(resourceGraphUri));
            if (path != null) cmdString.SetLiteral("path", pathGraphUri);
            var result = WrapRequest(() => QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            var dataIds = new List<string>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                cmdString = new SparqlParameterizedString
                {
                    CommandText = "SELECT ?v " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}"
                };
                if (resourceId != null) cmdString.SetUri("resource", new Uri(resourceGraphUri));
                if (path != null) cmdString.SetLiteral("path", pathGraphUri);
                using var results = WrapRequest(() => QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
                dataIds.AddRange(results.Select(x => x.Value("v").ToString()).ToList());
            }

            return dataIds;
        }

        /// <summary>
        /// Lists all current data versions
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="extracted"></param>
        /// <returns></returns>
        public IEnumerable<string?>? ListData(string? resourceId = null, bool extracted = false)
        {
            var dataIds = GetDataIds(resourceId, null, extracted);

            var results = dataIds
                .GroupBy((entry) => entry[..entry.LastIndexOf("/")])
                .Select((group) => group.Max());

            return results;
        }

        /// <summary>
        /// Gets the current metadata graph of a resource id and path combination.
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="path"></param>
        /// <param name="extracted"></param>
        /// <returns>Current metadata graph.</returns>
        public string? GetMetadataId(string resourceId, string path, bool extracted = false)
        {
            var metadataIds = GetMetadataIds(resourceId, path, extracted);

            return metadataIds.Max();
        }

        /// <summary>
        /// Gets all metadata graphs (versions) of a resource id and path combination.
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="path"></param>
        /// <param name="extracted"></param>
        /// <returns></returns>
        public List<string> GetMetadataIds(string? resourceId = null, string? path = null, bool extracted = false)
        {
            var resourceGraphUri = $"https://purl.org/coscine/resources/{resourceId}";
            var pathGraphUri = path != null
                ? new Uri($"{resourceGraphUri}/{(path.StartsWith("/") ? path[1..] : path)}/").AbsoluteUri
                : "";

            var query = $@"{{ 
                " + ((resourceId != null) ? $"@resource <{Uris.DcatCatalog}>* ?g ." : "") + $@"
                ?g a <{Uris.FdpMetadataService}> .
                ?g <{Uris.FdpHasMetadata}> ?m .
                ?m <{Uris.FdpHasMetadata}> ?v .
                " + ((path != null) ? "FILTER(contains(str(?m), @path)) ." : "") + $@"
                " + (extracted ? "FILTER(contains(str(?v), '&extracted=true')) ." : "FILTER(!contains(str(?v), '&extracted=true')) .") + $@"
            }}";

            var cmdString = new SparqlParameterizedString
            {
                CommandText = "SELECT COUNT(?v) AS ?count " + query
            };
            if (resourceId != null) cmdString.SetUri("resource", new Uri(resourceGraphUri));
            if (path != null) cmdString.SetLiteral("path", pathGraphUri);
            var result = WrapRequest(() => QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            var metadataIds = new List<string>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                cmdString = new SparqlParameterizedString
                {
                    CommandText = "SELECT ?v " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}"
                };
                if (resourceId != null) cmdString.SetUri("resource", new Uri(resourceGraphUri));
                if (path != null) cmdString.SetLiteral("path", pathGraphUri);
                using var results = WrapRequest(() => QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
                metadataIds.AddRange(results.Select(x => x.Value("v").ToString()).ToList());
            }

            return metadataIds;
        }

        /// <summary>
        /// Lists all current metadata versions
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="extracted"></param>
        /// <returns></returns>
        public IEnumerable<string?>? ListMetadata(string? resourceId = null, bool extracted = false)
        {
            var metadataIds = GetMetadataIds(resourceId, null, extracted);

            var results = metadataIds
                .GroupBy((entry) => entry[..entry.LastIndexOf("/")])
                .Select((group) => group.Max());

            return results;
        }

        /// <summary>
        /// Gets the recent metadata graph of a resource id and path combination.
        /// </summary>
        /// <returns>Recent metadata graph.</returns>
        public IGraph GetProvenanceGraph(Uri versionedGraph, string type = "metadata")
        {
            var filterString = $"@type={type}";

            var graphUriString = versionedGraph.AbsoluteUri;
            var provUriString = graphUriString[..(graphUriString.IndexOf(filterString) + filterString.Length)];

            return GetEmptySmallUpdateGraph(provUriString);
        }

        /// <summary>
        /// Add entry to Trellis
        /// </summary>
        /// <param name="ldpAssignment"></param>
        /// <param name="thePartUri"></param>
        /// <param name="graphUri"></param>
        /// <param name="trellisGraph">Optional: Either adds triples to given graph or saves to standard trellis graph</param>
        public void AddToTrellis(Uri ldpAssignment, string thePartUri, string graphUri, IGraph? trellisGraph = null)
        {
            var graphSet = trellisGraph != null;
            if (trellisGraph == null)
            {
                trellisGraph = GetEmptySmallUpdateGraph(Uris.TrellisGraph);
            }

            var setGraphNode = trellisGraph.CreateUriNode(new Uri(graphUri));
            var setThePartNode = trellisGraph.CreateUriNode(new Uri(thePartUri));
            var triple = new Triple(
                setGraphNode,
                trellisGraph.CreateUriNode(Uris.DctermsIsPartOf),
                setThePartNode
            );
            if (!trellisGraph.ContainsTriple(triple))
            {
                trellisGraph.Assert(triple);
                var assignmentTriple = new Triple(
                    setGraphNode,
                    trellisGraph.CreateUriNode(Uris.A),
                    trellisGraph.CreateUriNode(ldpAssignment)
                );
                trellisGraph.Assert(assignmentTriple);
                AddModifiedDate(trellisGraph, graphUri);
            }

            if (!graphSet)
            {
                AddGraph(trellisGraph);
            }
        }

        /// <summary>
        /// Adds a modified date to the given graph with the identifier of the given root, if none exists before
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="root"></param>
        public void AddModifiedDate(IGraph graph, string root)
        {
            var dcTermsModifiedNode = graph.CreateUriNode(Uris.DctermsModified);
            var rootNode = graph.CreateUriNode(new Uri(root));
            if (!graph.GetTriplesWithSubjectPredicate(rootNode, dcTermsModifiedNode).Any())
            {
                var triple = new Triple(
                    rootNode,
                    dcTermsModifiedNode,
                    graph.CreateLiteralNode(
                        DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture),
                        new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime)
                    )
                );
                graph.Assert(triple);
            }
        }

        /// <summary>
        /// Adds metadata to the platform with Provenance information attached
        /// </summary>
        /// <param name="graph">Metadata Graph</param>
        public async Task AddMetadataAsync(IGraph graph)
        {
            var graphUri = graph.BaseUri;
            var graphUriString = graphUri.AbsoluteUri;
            var afterBase = graphUriString.Replace("https://purl.org/coscine/resources/", "");
            var resourceId = afterBase[..afterBase.IndexOf("/")];
            var afterResource = afterBase[(afterBase.IndexOf("/") + 1)..];
            var path = afterResource[..afterResource.IndexOf("/@type")];

            var generalGraphName = graphUriString[..graphUriString.IndexOf("/@type")];

            var existingMetadataGraph = GetMetadataId(resourceId, path);

            var tasks = new List<Task>();

            if (!ListGraphsFromTrellisGraph(generalGraphName).Any())
            {
                var metadataGraphCreator = new MetadataGraphsCreator(this);
                var trellisGraph = GetEmptySmallUpdateGraph(Uris.TrellisGraph);
                var graphs = metadataGraphCreator.CreateGraphs(resourceId, path, true, false, new ProvidedGraphData { TrellisGraph = trellisGraph });
                foreach (var newGraph in graphs)
                {
                    tasks.Add(AddGraphAsync(newGraph));
                }
            }

            // Add Provenance information
            tasks.Add(AddProvenanceInformation(graphUri, graphUriString, generalGraphName, existingMetadataGraph));

            tasks.Add(AddGraphAsync(graph));

            await Task.WhenAll(tasks);
        }

        /// <summary>
        /// Retrieve
        /// </summary>
        /// <param name="generalGraphName"></param>
        /// <returns></returns>
        public IEnumerable<Uri> ListGraphsFromTrellisGraph(string generalGraphName)
        {
            var query = $@"WHERE {{ 
                GRAPH <{Uris.TrellisGraph}> {{ 
                    ?s a <{Uris.LdpRDFSource}> .
                    FILTER(contains(str(?s), @generalGraphName)) .
                }}
            }}";

            var cmdString = new SparqlParameterizedString
            {
                CommandText = "SELECT COUNT(?s) AS ?count " + query
            };
            cmdString.SetLiteral("generalGraphName", generalGraphName);
            var result = WrapRequest(() => QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
            var numberOfResult = Convert.ToInt32(((ILiteralNode)result.First().Value("count")).Value);

            var trellisIds = new List<Uri>();

            // iterates over results because a query limit exists
            for (var offset = 0; offset < numberOfResult; offset += QUERY_LIMIT)
            {
                cmdString = new SparqlParameterizedString
                {
                    CommandText = "SELECT ?s " + query + $" LIMIT {QUERY_LIMIT} OFFSET {offset}"
                };
                cmdString.SetLiteral("generalGraphName", generalGraphName);
                using var results = WrapRequest(() => QueryEndpoint.QueryWithResultSet(cmdString.ToString()));
                trellisIds.AddRange(results.Select(x => (x.Value("s") as IUriNode)!.Uri).ToList());
            }

            return trellisIds;
        }

        private async Task AddProvenanceInformation(Uri graphUri, string graphUriString, string generalGraphName, string? existingMetadataGraph)
        {
            var provenanceGraph = GetProvenanceGraph(graphUri);
            MetadataUtil.AssertToGraph(provenanceGraph, graphUri, Uris.A, Uris.ProvEntity);
            MetadataUtil.AssertToGraph(provenanceGraph, provenanceGraph.BaseUri, Uris.FdpHasMetadata, graphUri);
            MetadataUtil.AssertToGraph(provenanceGraph, graphUri, Uris.ProvGeneratedAtTime,
                DateTime.UtcNow.ToString("o", CultureInfo.InvariantCulture), new Uri(XmlSpecsHelper.XmlSchemaDataTypeDateTime));
            AddToTrellis(Uris.LdpRDFSource, generalGraphName, graphUriString);

            if (existingMetadataGraph != null && existingMetadataGraph != graphUriString)
            {
                MetadataUtil.AssertToGraph(provenanceGraph, graphUri, Uris.ProvWasRevisionOf, new Uri(existingMetadataGraph));
            }
            await AddGraphAsync(provenanceGraph);
        }

        /// <summary>
        /// Invalidates a metadata set in the given provenance graph
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="path"></param>
        /// <param name="type"></param>
        /// <param name="responsibleAgent"></param>
        public void SetInvalidation(string resourceId, string path, string type = "metadata", Uri? responsibleAgent = null)
        {
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }

            if (responsibleAgent is null)
            {
                responsibleAgent = Uris.CoscineUserAgent;
            }

            var urlId = new CoscineLDPHelper(this).GetId(resourceId, path, true, false, type);

            var provenanceGraph = GetEmptySmallUpdateGraph(
                $"https://purl.org/coscine/resources/{resourceId}{path}/@type={type}"
            );
            MetadataUtil.AssertToGraph(provenanceGraph, urlId, Uris.ProvWasInvalidatedBy, responsibleAgent);

            AddGraph(provenanceGraph);
        }
    }
}
