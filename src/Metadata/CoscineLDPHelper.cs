﻿using Coscine.Metadata.Util;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Metadata
{
    /// <summary>
    /// This class provides helpers for retrieving versions and ids for metadata
    /// </summary>
    public class CoscineLDPHelper
    {
        private readonly MetadataRdfStoreConnector _rdfStoreConnector;

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="rdfStoreConnector"></param>
        public CoscineLDPHelper(MetadataRdfStoreConnector rdfStoreConnector)
        {
            _rdfStoreConnector = rdfStoreConnector;
        }

        /// <summary>
        /// Gets the current metadata version
        /// </summary>
        /// <param name="metadataUrl"></param>
        /// <returns></returns>
        public Uri? GetCurrentMetadataVersion(Uri metadataUrl)
        {
            var respectiveGraphs = _rdfStoreConnector.ListGraphs(metadataUrl.AbsoluteUri);
            return VersionUtil.GetRecentMetadataVersion(respectiveGraphs);
        }

        /// <summary>
        /// Gets the current data version
        /// </summary>
        /// <param name="dataUrl"></param>
        /// <returns></returns>
        public Uri? GetCurrentDataVersion(Uri dataUrl)
        {
            var respectiveGraphs = _rdfStoreConnector.ListGraphs(dataUrl.AbsoluteUri);
            return VersionUtil.GetRecentDataVersion(respectiveGraphs);
        }

        /// <summary>
        /// Generates the main id
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public Uri GenerateMainId(string resourceId, string path)
        {
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }

            return new CustomUri($"https://purl.org/coscine/resources/{resourceId}{path}");
        }

        /// <summary>
        /// Generates a new id
        /// </summary>
        /// <param name="resourceId"></param>
        /// <param name="path"></param>
        /// <param name="type">data or metadata</param>
        /// <returns></returns>
        public Uri GenerateId(string resourceId, string path, string type = "metadata")
        {
            return new CustomUri($"{GenerateMainId(resourceId, path)}/@type={type}&version={VersionUtil.GetNewVersion()}");
        }

        /// <summary>
        /// Generates Id
        /// </summary>
        /// <param name="resourceId">Id of the resource</param>
        /// <param name="path">Path to file</param>
        /// <param name="retrieve">Is retrieve action?</param>
        /// <param name="storing">Is storing action?</param>
        /// <param name="type">data or metadata</param>
        /// <param name="givenIds">known ids</param>
        /// <returns> Uri </returns>
        public Uri GetId(string resourceId, string path, bool retrieve = false, bool storing = false, string type = "metadata", IEnumerable<string>? givenIds = null)
        {
            if (!path.StartsWith("/"))
            {
                path = "/" + path;
            }

            var searchString = resourceId + path;
            var currentId = givenIds?.FirstOrDefault((givenId) => givenId.Contains(searchString));
            if (currentId == null)
            {
                if (type == "data")
                {
                    currentId = _rdfStoreConnector.GetDataId(resourceId, path);
                }
                else
                {
                    currentId = _rdfStoreConnector.GetMetadataId(resourceId, path);
                }
            }

            if (currentId == null && retrieve)
            {
                return GenerateId(resourceId, path, type);
            }
            else if (currentId == null || storing)
            {
                return GenerateId(resourceId, path, type);
            }
            else
            {
                return new CustomUri(currentId);
            }
        }
    }
}
