﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using VDS.RDF;

namespace Coscine.Metadata.Util
{
    /// <summary>
    /// Provides utility functions to calculate hashes of research data
    /// </summary>
    public static class HashUtil
    {
        /// <summary>
        /// The default hash algorithm being used (SHA512)
        /// </summary>
        public static HashAlgorithmName DefaultHashAlgorithm => HashAlgorithmName.SHA512;

        private static HashAlgorithm GetHashAlgorithm(HashAlgorithmName hashAlgorithmName)
        {
            if (hashAlgorithmName == HashAlgorithmName.MD5)
                return MD5.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA1)
                return SHA1.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA256)
                return SHA256.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA384)
                return SHA384.Create();
            if (hashAlgorithmName == HashAlgorithmName.SHA512)
                return SHA512.Create();

            throw new CryptographicException($"Unknown hash algorithm \"{hashAlgorithmName.Name}\".");
        }

        /// <summary>
        /// Takes a data stream and returns a hash represnetation of it
        /// </summary>
        /// <param name="data"></param>
        /// <param name="hashAlgorithmName"></param>
        /// <returns></returns>
        public static byte[] HashData(Stream data, HashAlgorithmName? hashAlgorithmName = null)
        {
            hashAlgorithmName ??= DefaultHashAlgorithm;
            using var hashAlgorithmObject = GetHashAlgorithm(hashAlgorithmName.Value);
            return hashAlgorithmObject.ComputeHash(data);
        }

        /// <summary>
        /// Creates triples which identify a research data's metadata version by their hash representation
        /// </summary>
        /// <param name="dataGraph"></param>
        /// <param name="dataGraphId"></param>
        /// <param name="hashValue"></param>
        /// <param name="hashAlgorithmName"></param>
        /// <param name="hashGraphId"></param>
        /// <returns></returns>
        public static IEnumerable<Triple> CreateHashTriples(
            IGraph dataGraph,
            Uri dataGraphId,
            string hashValue,
            HashAlgorithmName? hashAlgorithmName = null,
            Uri? hashGraphId = null
        )
        {
            hashAlgorithmName ??= DefaultHashAlgorithm;
            hashGraphId ??= new Uri($"{dataGraphId}&hash={Guid.NewGuid()}");

            var hashTriples = new List<Triple>();

            var dataGraphSubject = dataGraph.CreateUriNode(dataGraphId);
            var hashSubject = dataGraph.CreateUriNode(hashGraphId);

            hashTriples.Add(new Triple(dataGraphSubject,
                            dataGraph.CreateUriNode(Uris.EbocoreHashType),
                            hashSubject));
            hashTriples.Add(new Triple(hashSubject,
                                        dataGraph.CreateUriNode(Uris.EbocoreHashFunction),
                                        dataGraph.CreateLiteralNode(hashAlgorithmName.Value.Name)));
            hashTriples.Add(new Triple(hashSubject,
                                        dataGraph.CreateUriNode(Uris.EbocoreHashValue),
                                        dataGraph.CreateLiteralNode(hashValue, Uris.XsdHexBinary)));

            return hashTriples;
        }
    }
}
