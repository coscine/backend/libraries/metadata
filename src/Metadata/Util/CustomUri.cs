﻿using System;

namespace Coscine.Metadata.Util
{
    /// <summary>
    /// Adapts the returned ToString of a Uri
    /// </summary>
    public class CustomUri : Uri
    {
        /// <summary>
        /// Constructs a Custom Uri
        /// </summary>
        /// <param name="uri"></param>
        public CustomUri(string uri) : base(uri)
        {
        }

        /// <summary>
        /// Overwrites the ToString for returning the AbsoluteUri (for dotNetRDF)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return AbsoluteUri;
        }
    }
}
