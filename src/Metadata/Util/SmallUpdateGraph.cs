﻿using System.Collections.Generic;
using VDS.RDF;

namespace Coscine.Metadata.Util
{
    /// <summary>
    /// Provides an implementation of a graph with an assertion and retraction list
    /// </summary>
    public class SmallUpdateGraph : Graph
    {
        /// <summary>
        /// List of everything asserted to a graph
        /// </summary>
        public List<Triple> AssertList { get; set; }

        /// <summary>
        /// List of everything retracted from a graph
        /// </summary>
        public List<Triple> RetractList { get; set; }

        /// <summary>
        /// Default constructor
        /// </summary>
        public SmallUpdateGraph() : base()
        {
            AssertList = new List<Triple>();
            RetractList = new List<Triple>();
        }

        /// <summary>
        /// Asserts a Triple in the graph, additionally adds it to the AssertList.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public override bool Assert(Triple t)
        {
            AssertList.Add(t);
            return base.Assert(t);
        }

        /// <summary>
        /// Asserts triples in the graph, additionally adds them to the AssertList.
        /// </summary>
        /// <param name="triples"></param>
        /// <returns></returns>
        public override bool Assert(IEnumerable<Triple> triples)
        {
            AssertList.AddRange(triples);
            return base.Assert(triples);
        }

        /// <summary>
        /// Retracts a Triple from the graph, additionally adds it to the RetractList.
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public override bool Retract(Triple t)
        {
            RetractList.Add(t);
            return base.Retract(t);
        }

        /// <summary>
        /// Retracts triples from the graph, additionally adds them to the RetractList.
        /// </summary>
        /// <param name="triples"></param>
        /// <returns></returns>
        public override bool Retract(IEnumerable<Triple> triples)
        {
            RetractList.AddRange(triples);
            return base.Retract(triples);
        }
    }
}
