﻿using Coscine.Configuration;
using Coscine.Metadata.Util;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Linq;
using VDS.RDF;
using VDS.RDF.Parsing;

namespace Coscine.Metadata.Tests
{
    [TestFixture]
    public class MetadataTests
    {
        private readonly IConfiguration _configuration = new ConsulConfiguration();

        private RdfStoreConnector _rdfStoreConnector;
        private CoscineLDPHelper _coscineLDPHelper;

        private readonly string testResourceId = Guid.NewGuid().ToString();
        private readonly string testFileName = "test.txt";

        [OneTimeSetUp]
        public void Init()
        {
            _rdfStoreConnector = new RdfStoreConnector(_configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
            _coscineLDPHelper = new CoscineLDPHelper(_rdfStoreConnector);
        }

        [Test]
        public void MetadataLifeCycleTest()
        {
            var newUri = _coscineLDPHelper.GenerateId(testResourceId, testFileName, "metadata");
            Assert.IsFalse(_rdfStoreConnector.HasGraph(newUri));

            var metadata = @$"@base <{newUri}> .

@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix aps: <https://purl.org/coscine/ap/> .

_:b1 a <https://purl.org/coscine/ap/radar/> ;
	dcterms:creator ""Tester"" ;
	dcterms:title ""Test"" ;
	dcterms:created ""2022-12-12""^^xsd:date .
";
            var graph = _rdfStoreConnector.ParseGraph(metadata, new TurtleParser());
            _rdfStoreConnector.AddMetadataAsync(graph).Wait();

            var metadataId = _rdfStoreConnector.GetMetadataId(testResourceId, testFileName);
            Assert.IsNotNull(metadataId);
            Assert.IsTrue(_rdfStoreConnector.HasGraph(metadataId));
            var newGraph = _rdfStoreConnector.GetGraph(metadataId);
            Assert.IsTrue(newGraph.Triples.Count == graph.Triples.Count);
        }

        [Test]
        public void DataLifeCycleTest()
        {
            var newUri = _coscineLDPHelper.GenerateId(testResourceId, testFileName, "metadata");
            Assert.IsFalse(_rdfStoreConnector.HasGraph(newUri));

            var metadata = @$"@base <{newUri}> .

@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix aps: <https://purl.org/coscine/ap/> .

_:b1 a <https://purl.org/coscine/ap/radar/> ;
	dcterms:creator ""Tester"" ;
	dcterms:title ""Test"" ;
	dcterms:created ""2022-12-12""^^xsd:date .
";
            var metadatagraph = _rdfStoreConnector.ParseGraph(metadata, new TurtleParser());
            _rdfStoreConnector.AddMetadataAsync(metadatagraph).Wait();

            var urlId = _coscineLDPHelper.GetId(testResourceId, testFileName, true, false, "data");

            var graph = _rdfStoreConnector.GetGraph(urlId);

            if (!graph.IsEmpty)
            {
                var triples = graph.GetTriplesWithPredicate(graph.CreateUriNode(Uris.CoscineLinkedBody));
                graph.Retract(triples.ToArray());
            }
            else
            {
                _rdfStoreConnector.CreateNamedGraph(urlId);
            }

            graph.Assert(new Triple(graph.CreateUriNode(urlId), graph.CreateUriNode(Uris.CoscineLinkedBody), graph.CreateLiteralNode("www.example.com")));

            _rdfStoreConnector.AddGraph(graph);

            var dataId = _rdfStoreConnector.GetDataId(testResourceId, testFileName);
            Assert.IsNotNull(dataId);
            Assert.IsTrue(_rdfStoreConnector.HasGraph(dataId));
            var newGraph = _rdfStoreConnector.GetGraph(dataId);
            Assert.IsTrue(newGraph.Triples.Count == graph.Triples.Count);

            urlId = _coscineLDPHelper.GetId(testResourceId, testFileName, true, false, "data");

            var provenanceGraph = _rdfStoreConnector.GetEmptySmallUpdateGraph(
                $"https://purl.org/coscine/resources/{testResourceId}{testFileName}/@type=data"
            );
            MetadataUtil.AssertToGraph(provenanceGraph, urlId, Uris.ProvWasInvalidatedBy, Uris.CoscineUserAgent);
            _rdfStoreConnector.AddGraph(provenanceGraph);

            dataId = _rdfStoreConnector.GetDataId(testResourceId, testFileName);
            Assert.IsNull(dataId);
        }
    }
}
