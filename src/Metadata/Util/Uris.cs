﻿using System;

namespace Coscine.Metadata.Util
{
    /// <summary>
    /// Helper class for common URIs of ontologies
    /// </summary>
    public static class Uris
    {
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

        public static readonly Uri OrgPrefix = new("http://www.w3.org/ns/org#");
        public static readonly Uri FoafPrefix = new("http://xmlns.com/foaf/0.1/");

        public static readonly Uri A = new("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");

        // Coscine
        public static readonly Uri CoscineLinkedBody = new("https://purl.org/coscine/terms/linked#body");
        public static readonly Uri CoscineMetadataTrackerAgent = new("https://purl.org/coscine/terms/metadatatracker#Agent");
        public static readonly Uri CoscineUserAgent = new("https://purl.org/coscine/terms/user#Agent");
        public static readonly Uri CoscineResourcePrefix = new("https://purl.org/coscine/resources");
        public static readonly Uri CoscineMetadataExtractionEntity = new("https://purl.org/coscine/terms/metatadataextraction#Entity");
        public static readonly Uri CoscineMetadataExtractionExtracted = new("https://purl.org/coscine/terms/metatadataextraction#extracted");
        public static readonly Uri CoscineMetadataExtractionVersion = new("https://purl.org/coscine/terms/metatadataextraction#version");

        // Dcat
        public static readonly Uri DcatCatalog = new("http://www.w3.org/ns/dcat#catalog");
        public static readonly Uri DcatCatalogClass = new("http://www.w3.org/ns/dcat#Catalog");
        public static readonly Uri DcatDataset = new("http://www.w3.org/ns/dcat#dataset");
        public static readonly Uri DcatDatasetClass = new("http://www.w3.org/ns/dcat#Dataset");
        public static readonly Uri DcatDistribution = new("http://www.w3.org/ns/dcat#distribution");

        // Dcterms
        public static readonly Uri DctermsIdentifier = new("http://purl.org/dc/terms/identifier");
        public static readonly Uri DctermsIsPartOf = new("http://purl.org/dc/terms/isPartOf");
        public static readonly Uri DctermsModified = new("http://purl.org/dc/terms/modified");

        // Ebucore
        public static readonly Uri EbocoreHashFunction = new("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashFunction");
        public static readonly Uri EbocoreHashType = new("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashType");
        public static readonly Uri EbocoreHashValue = new("http://www.ebu.ch/metadata/ontologies/ebucore/ebucore#hashValue");

        // FDP
        public static readonly Uri FdpMetadataService = new("http://purl.org/fdp/fdp-o#MetadataService");
        public static readonly Uri FdpHasMetadata = new("http://purl.org/fdp/fdp-o#hasMetadata");
        public static readonly Uri FdpIsMetadataOf = new("http://purl.org/fdp/fdp-o#isMetadataOf");

        // LDP
        public static readonly Uri LdpBasicContainer = new("http://www.w3.org/ns/ldp#BasicContainer");
        public static readonly Uri LdpNonRDFSource = new("http://www.w3.org/ns/ldp#NonRDFSource");
        public static readonly Uri LdpRDFSource = new("http://www.w3.org/ns/ldp#RDFSource");
        public static readonly Uri LdpDescribedBy = new("http://www.w3.org/ns/ldp#describedBy");

        // PROV
        public static readonly Uri ProvEntity = new("http://www.w3.org/ns/prov#Entity");
        public static readonly Uri ProvGeneratedAtTime = new("http://www.w3.org/ns/prov#generatedAtTime");
        public static readonly Uri ProvWasInvalidatedBy = new("http://www.w3.org/ns/prov#wasInvalidatedBy");
        public static readonly Uri ProvWasRevisionOf = new("http://www.w3.org/ns/prov#wasRevisionOf");

        // Trellis
        public static readonly Uri TrellisGraph = new("http://www.trellisldp.org/ns/trellis#PreferServerManaged");

        // Xsd
        public static readonly Uri XsdHexBinary = new("http://www.w3.org/2001/XMLSchema#hexBinary");

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}
