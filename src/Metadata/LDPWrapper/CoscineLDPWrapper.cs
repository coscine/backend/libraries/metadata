﻿using Coscine.Configuration;
using Coscine.Metadata.Util;
using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Writing;

namespace Coscine.Metadata.LDPWrapper
{
    /// <summary>
    /// WIP Implementation of an implementation which interacts with Linked Data Platforms
    /// </summary>
    public class CoscineLDPWrapper : ILDPWrapper
    {
        // Will be documented when fully implemented
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly string _endpoint;

        public string? JWT { get; set; }

        public CoscineLDPWrapper(IConfiguration configuration, HttpClient httpClient, bool wrapperUse = true, string? jwt = null)
        {
            _configuration = configuration;
            _httpClient = httpClient;
            _endpoint = wrapperUse
                ? _configuration.GetString("coscine/local/ldpwrapper/url", "http://localhost:5000/")
                : _configuration.GetString("coscine/local/trellis/url", "http://localhost:8253/");
            JWT = jwt;
        }

        public async Task<string?> Add(string container, string identifier, IGraph? body = null)
        {
            var message = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri($"{_endpoint}{container}")
            };

            if (body != null)
            {
                message.Content = new StringContent(VDS.RDF.Writing.StringWriter.Write(body, new CompressingTurtleWriter()));
                message.Content.Headers.ContentType = new MediaTypeHeaderValue("text/turtle");
            }

            message.Headers.Add("Accept", "text/turtle");
            message.Headers.Add("Link", "<http://www.w3.org/ns/ldp#Resource>; rel=\"type\"");
            message.Headers.Add("Slug", identifier);

            return ParseIdentifier(
                await FireRequest(message)
            );
        }

        public async Task<string?> Create(string identifier, IGraph? body = null)
        {
            var message = new HttpRequestMessage()
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri($"{_endpoint}")
            };

            if (body != null)
            {
                message.Content = new StringContent(VDS.RDF.Writing.StringWriter.Write(body, new CompressingTurtleWriter()));
                message.Content.Headers.ContentType = new MediaTypeHeaderValue("text/turtle");
            }

            message.Headers.Add("Accept", "text/turtle");
            message.Headers.Add("Link", "<http://www.w3.org/ns/ldp#BasicContainer>; rel=\"type\"");
            message.Headers.Add("Slug", identifier);

            return ParseIdentifier(
                await FireRequest(message)
            );
        }

        public Task<bool> Delete(string identifier)
        {
            throw new NotImplementedException();
        }

        public async Task<IGraph?> Get(string identifier)
        {
            var message = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{_endpoint}{identifier}")
            };

            message.Headers.Add("Accept", "text/turtle");

            return await ParseGraph(
                await FireRequest(message)
            );
        }

        public async Task<bool> Exists(string identifier)
        {
            var message = new HttpRequestMessage()
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri($"{_endpoint}{identifier}")
            };

            message.Headers.Add("Accept", "text/turtle");

            return (await FireRequest(message)).IsSuccessStatusCode;
        }

        public async Task<bool> SetAuthorization(string identifier, IGraph? body = null)
        {
            return await Update(identifier + "/?ext=acl", body, LinkType.Authorization);
        }

        public async Task<bool> Update(string identifier, IGraph? body = null, LinkType? linkType = null)
        {
            var message = new HttpRequestMessage()
            {
                Method = HttpMethod.Put,
                RequestUri = new Uri($"{_endpoint}{identifier}")
            };

            if (body != null)
            {
                message.Content = new StringContent(VDS.RDF.Writing.StringWriter.Write(body, new CompressingTurtleWriter()));
                message.Content.Headers.ContentType = new MediaTypeHeaderValue("text/turtle");
            }

            message.Headers.Add("Accept", "text/turtle");

            if (linkType != null)
            {
                switch (linkType)
                {
                    case LinkType.Container:
                        message.Headers.Add("Link", "<http://www.w3.org/ns/ldp#BasicContainer>; rel=\"type\"");
                        break;
                    case LinkType.Resource:
                        message.Headers.Add("Link", "<http://www.w3.org/ns/ldp#Resource>; rel=\"type\"");
                        break;
                    case LinkType.Authorization:
                        message.Headers.Add("Link", "<http://www.w3.org/ns/auth/acl#Authorization>; rel=\"type\"");
                        break;
                }
            }

            return (await FireRequest(message)).IsSuccessStatusCode;
        }

        private async Task<HttpResponseMessage> FireRequest(HttpRequestMessage message)
        {
            if (JWT != null)
            {
                message.Headers.Add("Authorization", $"Bearer {JWT}");
            }

            return await _httpClient.SendAsync(message);
        }

        private async Task<IGraph?> ParseGraph(HttpResponseMessage response)
        {
            var graphString = await response.Content.ReadAsStringAsync();

            try
            {
                var graph = new Graph();
                var ttlparser = new TurtleParser();
                ttlparser.Load(graph, new StringReader(graphString));

                return graph;
            }
            catch
            {
                return null;
            }
        }

        private string? ParseIdentifier(HttpResponseMessage response)
        {
            return response.Headers.GetValues("location").FirstOrDefault();
        }

#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member
    }
}