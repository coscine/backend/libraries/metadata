﻿using System;
using VDS.RDF;

namespace Coscine.Metadata.Util
{
    /// <summary>
    /// Utility functions which simplify the interactions with graphs
    /// </summary>
    public static class MetadataUtil
    {
        /// <summary>
        /// Asserts the given parameters to a graph
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="subject"></param>
        /// <param name="predicate"></param>
        /// <param name="uriObject"></param>
        public static void AssertToGraph(IGraph graph, string subject, string predicate, Uri uriObject)
        {
            AssertToGraph(graph, new Uri(subject), new Uri(predicate), uriObject);
        }

        /// <summary>
        /// Asserts the given parameters to a graph
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="subject"></param>
        /// <param name="predicate"></param>
        /// <param name="uriObject"></param>
        public static void AssertToGraph(IGraph graph, Uri subject, Uri predicate, Uri uriObject)
        {
            graph.Assert(
                new Triple(
                    graph.CreateUriNode(subject),
                    graph.CreateUriNode(predicate),
                    graph.CreateUriNode(uriObject)
                )
            );
        }

        /// <summary>
        /// Asserts the given parameters to a graph
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="subject"></param>
        /// <param name="predicate"></param>
        /// <param name="literalObject"></param>
        /// <param name="objectType"></param>
        public static void AssertToGraph(IGraph graph, string subject, string predicate, string literalObject, Uri? objectType = null)
        {
            AssertToGraph(graph, new Uri(subject), new Uri(predicate), literalObject, objectType);
        }

        /// <summary>
        /// Asserts the given parameters to a graph
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="subject"></param>
        /// <param name="predicate"></param>
        /// <param name="literalObject"></param>
        /// <param name="objectType"></param>
        public static void AssertToGraph(IGraph graph, Uri subject, Uri predicate, string literalObject, Uri? objectType = null)
        {
            graph.Assert(
                new Triple(
                    graph.CreateUriNode(subject),
                    graph.CreateUriNode(predicate),
                    objectType != null ? graph.CreateLiteralNode(literalObject, objectType) : graph.CreateLiteralNode(literalObject)
                )
            );
        }
    }
}
