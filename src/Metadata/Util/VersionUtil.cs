﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Metadata.Util
{
    /// <summary>
    /// Utility for determining versions
    /// </summary>
    public static class VersionUtil
    {
        /// <summary>
        /// The maximum file size for research data which should be analyzed by e.g. Metadata Extractor or Tracker
        /// </summary>
        public static long DetectionByteLimit => 16 * 1000 * 1000;

        /// <summary>
        /// Receives the recent version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <param name="filter"></param>
        /// <param name="notFilterExtracted"></param>
        /// <returns></returns>
        public static Uri? GetRecentVersion(IEnumerable<Uri>? graphs, string? filter = null, bool notFilterExtracted = true)
        {
            if (graphs is null)
            {
                return null;
            }
            var recentVersion = GetRecentVersion(graphs.Select((graph) => graph.AbsoluteUri), filter, notFilterExtracted);
            if (recentVersion is not null)
            {
                return new Uri(recentVersion);
            }
            return null;
        }

        /// <summary>
        /// Receives the recent version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <param name="filter"></param>
        /// <param name="notFilterExtracted"></param>
        /// <returns></returns>
        public static string? GetRecentVersion(IEnumerable<string>? graphs, string? filter = null, bool notFilterExtracted = true)
        {
            if (graphs is null)
            {
                return null;
            }
            string? currentBest = null;
            var currentBestVersion = -1L;
            foreach (var graph in graphs)
            {
                var queryDictionary = System.Web.HttpUtility.ParseQueryString(
                    new Uri(graph.Replace("@", "?")).Query);
                var version = queryDictionary["version"];
                if (version is null || !long.TryParse(version, out long longVersion))
                {
                    continue;
                }
                if (longVersion > currentBestVersion
                    && (filter is null || queryDictionary["type"] == filter)
                    && ((notFilterExtracted && queryDictionary["extracted"] is null)
                        || (!notFilterExtracted && queryDictionary["extracted"] is not null))
                )
                {
                    currentBestVersion = longVersion;
                    currentBest = graph;
                }
            }
            return currentBest;
        }

        /// <summary>
        /// Receives the recent data version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static Uri? GetRecentDataVersion(IEnumerable<Uri>? graphs)
        {
            return GetRecentVersion(graphs, "data");
        }

        /// <summary>
        /// Receives the recent data version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static string? GetRecentDataVersion(IEnumerable<string>? graphs)
        {
            return GetRecentVersion(graphs, "data");
        }

        /// <summary>
        /// Receives the recent extracted data version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static Uri? GetRecentDataExtractedVersion(IEnumerable<Uri> graphs)
        {
            return GetRecentVersion(graphs, "data", false);
        }

        /// <summary>
        /// Receives the recent extracted data version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static string? GetRecentDataExtractedVersion(IEnumerable<string> graphs)
        {
            return GetRecentVersion(graphs, "data", false);
        }

        /// <summary>
        /// Receives the recent metadata version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static Uri? GetRecentMetadataVersion(IEnumerable<Uri>? graphs)
        {
            return GetRecentVersion(graphs, "metadata");
        }

        /// <summary>
        /// Receives the recent metadata version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static string? GetRecentMetadataVersion(IEnumerable<string>? graphs)
        {
            return GetRecentVersion(graphs, "metadata");
        }

        /// <summary>
        /// Receives the recent extracted metadata version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static Uri? GetRecentMetadataExtractedVersion(IEnumerable<Uri>? graphs)
        {
            return GetRecentVersion(graphs, "metadata", false);
        }

        /// <summary>
        /// Receives the recent extracted metadata version by the given parameters
        /// </summary>
        /// <param name="graphs"></param>
        /// <returns></returns>
        public static string? GetRecentMetadataExtractedVersion(IEnumerable<string>? graphs)
        {
            return GetRecentVersion(graphs, "metadata", false);
        }

        /// <summary>
        /// Creates a new version with the current timestamp
        /// </summary>
        /// <returns></returns>
        public static long GetNewVersion()
        {
            // UTC Timestamp
            return CalculateVersion(DateTime.UtcNow);
        }

        /// <summary>
        /// Calculates a new version based on the given timestamp
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static long CalculateVersion(DateTime dateTime)
        {
            return long.Parse(Convert.ToString((int)dateTime.Subtract(new DateTime(1970, 1, 1)).TotalSeconds));
        }

        /// <summary>
        /// Gets the version from the given graphUri
        /// </summary>
        /// <param name="graphUri">string representation of the given graph uri</param>
        /// <returns></returns>
        public static long? GetVersion(string? graphUri)
        {
            if (graphUri is null)
            {
                return null;
            }
            var queryDictionary = System.Web.HttpUtility.ParseQueryString(
                new Uri(graphUri.Replace("@", "?")).Query);
            var version = queryDictionary["version"];
            var couldParse = long.TryParse(version, out long longVersion);
            return couldParse ? longVersion : null;
        }

        /// <summary>
        /// Gets the version from the given graphUri
        /// </summary>
        /// <param name="graphUri"></param>
        /// <returns></returns>
        public static long? GetVersion(Uri? graphUri)
        {
            if (graphUri is null)
            {
                return null;
            }
            return GetVersion(graphUri.AbsoluteUri);
        }
    }
}
