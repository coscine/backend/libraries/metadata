﻿using Coscine.Metadata.Util;
using System.Threading.Tasks;
using VDS.RDF;

namespace Coscine.Metadata.LDPWrapper
{
    /// <summary>
    /// Interface for a wrapper which interacts with Linked Data Platforms
    /// </summary>
    public interface ILDPWrapper
    {
        /// <summary>
        /// Gets a graph by identifier
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public Task<IGraph?> Get(string identifier);
        /// <summary>
        /// Checks if an identifier exists
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public Task<bool> Exists(string identifier);
        /// <summary>
        /// Deletes by identifier
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public Task<bool> Delete(string identifier);
        /// <summary>
        /// Adds a container with its identifier. The body is the stored content as a graph.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="identifier"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public Task<string?> Add(string container, string identifier, IGraph? body = null);
        /// <summary>
        /// Creates an entity by an identifier. The body is the stored content as a graph.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public Task<string?> Create(string identifier, IGraph? body = null);
        /// <summary>
        /// Updates an entity by an identifier. The body is the stored content as a graph.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="body"></param>
        /// <param name="linkType"></param>
        /// <returns></returns>
        public Task<bool> Update(string identifier, IGraph? body = null, LinkType? linkType = null);
        /// <summary>
        /// Sets the authorization strategy of an identifier. The body is the stored strategy as a graph.
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public Task<bool> SetAuthorization(string identifier, IGraph? body = null);
    }
}
