﻿using Coscine.Configuration;
using NUnit.Framework;
using System;
using VDS.RDF.Parsing;

namespace Coscine.Metadata.Tests
{
    [TestFixture]
    public class GenericTests
    {
        private readonly IConfiguration _configuration = new ConsulConfiguration();

        private RdfStoreConnector _rdfStoreConnector;

        [OneTimeSetUp]
        public void Init()
        {
            _rdfStoreConnector = new RdfStoreConnector(_configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
        }

        [Test]
        public void HasGraphTest()
        {
            Assert.IsTrue(_rdfStoreConnector.HasGraph("http://www.openlinksw.com/schemas/virtrdf#"));
            Assert.IsTrue(_rdfStoreConnector.HasGraph("http://localhost:8890/DAV/"));
            Assert.IsFalse(_rdfStoreConnector.HasGraph("https://test.de/fdgdhjklö/"));
        }

        [Test]
        public void GetGraphTest()
        {
            Assert.IsFalse(_rdfStoreConnector.GetGraph("http://www.openlinksw.com/schemas/virtrdf#").IsEmpty);
            Assert.IsFalse(_rdfStoreConnector.GetGraph("http://localhost:8890/DAV/").IsEmpty);
            // Returns an empty graph, if the graph does not exists
            Assert.IsTrue(_rdfStoreConnector.GetGraph("https://test.de/fdgdhjklö/").IsEmpty);
        }

        [Test]
        public void GetOrganizations()
        {
            Assert.NotNull(_rdfStoreConnector.GetTriplesByObject(null, null, new Uri("http://www.w3.org/ns/org#FormalOrganization")));
        }

        [Test]
        public void TestValidation()
        {
            string metadata = "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>.\r\n@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>.\r\n@prefix xsd: <http://www.w3.org/2001/XMLSchema#>.\r\n\r\n<https://hdl.handle.net/11148/865c34ff-0b34-49ce-a3cb-9e63026e540d@path=%2Findex.ttl> <http://purl.obolibrary.org/obo/NCIT_C12801> \"Heart\";\r\n                                                                                      <http://purl.obolibrary.org/obo/NCIT_C25219> \"wer\";\r\n                                                                                      <http://purl.obolibrary.org/obo/NCIT_C42790> \"awt\";\r\n                                                                                      <http://purl.obolibrary.org/obo/NCIT_C45293> \"Human\";\r\n                                                                                      <http://purl.obolibrary.org/obo/NCIT_C48163> \"FFPE\";\r\n                                                                                      <http://purl.obolibrary.org/obo/NCIT_C80225> \"eweraerawrawerawerwearawerwaerawerawerer\";\r\n                                                                                      <http://purl.obolibrary.org/obo/SCDO_0002829> \"awt\";\r\n                                                                                      <http://purl.org/dc/terms/created> \"2023-01-18\"^^xsd:date;\r\n                                                                                      <http://purl.org/dc/terms/creator> \"Benedikt Heinrichs\";\r\n                                                                                      a <https://purl.org/coscine/ap/kuppelab/merfish/>;\r\n                                                                                      <https://purl.org/coscine/terms/4978e908-8918-4022-aed6-f295a5d6a2d8/> \"wer\".\r\n";
            Uri graphNameUri = new("https://hdl.handle.net/11148/865c34ff-0b34-49ce-a3cb-9e63026e540d@path=%2Findex.ttl");

            var graph = _rdfStoreConnector.ParseGraph(metadata, new TurtleParser());
            graph.BaseUri = new Uri("https://hdl.handle.net/11148/865c34ff-0b34-49ce-a3cb-9e63026e540d@path=%2Findex.ttl");

            Assert.IsTrue(_rdfStoreConnector.ValidateShacl(graph, graphNameUri, new Uri("https://purl.org/coscine/ap/kuppelab/merfish/1669378490042/")));
        }
    }
}