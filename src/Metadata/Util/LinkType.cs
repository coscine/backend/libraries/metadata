﻿namespace Coscine.Metadata.Util
{
    /// <summary>
    /// An enum representing all Linked Data Types (Container, Resource) and Authorization
    /// </summary>
    public enum LinkType
    {
        /// <summary>
        /// LDP Container
        /// </summary>
        Container,
        /// <summary>
        /// LDP Resource
        /// </summary>
        Resource,
        /// <summary>
        /// WAC Authorization strategy
        /// </summary>
        Authorization
    }
}
